import XCTest
import Foundation
@testable import stringdeallocreproducer

final class stringdeallocreproducerTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // this crashes with Foundation/NSCFString.swift:119: Fatal error: Constant strings cannot be deallocated
        let url = Bundle.module.url(forResource: "junkfile", withExtension: "txt", subdirectory: "Resources")
    }
    func testExample() throws {
        print("this is here to force a setup call")
    }
}
