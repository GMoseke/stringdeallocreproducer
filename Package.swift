// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "stringdeallocreproducer",
    products: [
        .library(
            name: "stringdeallocreproducer",
            targets: ["stringdeallocreproducer"]),
    ],
    targets: [
        .target(
            name: "stringdeallocreproducer",
            dependencies: []),
        .testTarget(
            name: "stringdeallocreproducerTests",
            dependencies: ["stringdeallocreproducer"],
            path: "Tests",
            resources: [.copy("Resources")]),
    ]
)
