Small reproducer for NSCFString dealloc bug when using `Bundle.module.url` on linux nightly toolchains 04-23 and later
Specifically:

```swift
// This throws `Foundation/NSCFString.swift:119: Fatal error: Constant strings cannot be deallocated`
Bundle.module.url(forResource: "somefile", withExtension: "txt", subdirectory: "Resources")
```

To test:
```shell
docker run -it $(docker build -q .)
```